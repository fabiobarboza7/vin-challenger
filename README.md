# Short Code Exercise

### Description

It's a challenge to test ruby skills. The test is to get an initial Ruby script and improve upon it. Attempts to check if the calculated check digit matches the input. 

_* The full description about the test can be found in `short_coding_exercise.pdf` file._

VIN concepts is based on https://en.wikipedia.org/wiki/Vehicle_identification_number#World_manufacturer_identifier.

##### How to run:

Download the file `vin_validator.rb` and in your terminal run:

```
➜ ruby vin_validator.rb <VIN-CODE-TO-TEST>
```

##### Example:

```
➜ ruby vin_validator.rb RNKDLUDX93R385016
```

#### Requerid Parameters

* `VIN-CODE-TO-TEST` -  Should be according to https://en.wikipedia.org/wiki/Vehicle_identification_number
 
#### Output example

```
➜ ruby vin_validator.rb 3HSDJAPR5FN657165                 

Provided VIN: 3HSDJAPR5FN657165
Check Digit: Valid
This looks like a VALID vin!

Generate a report in your current path?
[y] for yes 
[n] for no
y
Report created, check in your current path for report_1611559631.txt file.
```
