class VinValidator
	TRANSLITERATE = {
	  "A" => 1, "B" => 2, "C" => 3, "D" => 4,
	  "E" => 5, "F" => 6, "G" => 7, "H" => 8,
	  "J" => 1, "K" => 2, "L" => 3, "M" => 4,
	  "N" => 5, "P" => 7, "R" => 9, "S" => 2,
	  "T" => 3, "U" => 4, "V" => 5, "W" => 6,
	  "X" => 7, "Y" => 8, "Z" => 9
	}  

	WEIGHTS = [
							8, 7, 6, 5, 4, 3, 2, 10, 
							0, 9, 8, 7, 6, 5, 4, 3, 2
						]

	FORBIDDEN_CHARS = ["I", "O", "Q"]

	def initialize(vin)
		@vin = vin.upcase if vin
	end

	def generate_report(message = '', data = '')
		puts "\nGenerate a report in your current path?"
		puts "[y] for yes \n[n] for no"
		
		user_warning = STDIN.gets.chomp
		
		case user_warning.downcase
		when 'y'
			created_at = Time.now
			File.open("report_#{created_at.to_i}.txt", "w") do |f|     
			  f.write("Report created in #{created_at}\n#{message}\n#{data}")   
			end

			puts "\033[35mReport created, check in your current path for report_#{created_at.to_i}.txt file."
		when 'n'
		else
			puts "Invalid choice, report not generated"
		end
	end

	def is_valid(char) /[A-HJ-NPR-Z0-9]/ === char end
	def is_i(char) /\A[-+]?\d+\z/ === char end

	def is_not_valid
		@vin.split('').any? { |each| FORBIDDEN_CHARS.include? each  }
	end

	def random_valid_character
		allowed_char = ("A".."Z").to_a + ("0".."9").to_a - FORBIDDEN_CHARS

		allowed_char.sample
	end

	def generate_suggested_vin
		suggest_vin = @vin.dup

		@vin.split('').each_with_index do |each, index|
			if FORBIDDEN_CHARS.include? each
				suggest_vin[index] = random_valid_character
			end
		end

		suggest_vin
	end

	def calculate_check_digit(vin_params)
		error_message = false
		forbidden_chars = []
		check_digit = (0..10).to_a
		sum = 0
		ignore_sum = false

		vin_params.split('').each_with_index do |char, index|
			if !is_valid(char)
				ignore_sum = true
				forbidden_chars << char
				error_message = "#{forbidden_chars.uniq} is not a valid character"
			end

			if !ignore_sum
				unless is_i(char)
					sum += TRANSLITERATE[char] * WEIGHTS[index]
				else
					sum += char.to_i * WEIGHTS[index]
				end
			end
		end
		
		calculation = check_digit[sum % 11].to_s

		result = calculation == "10" ? "X" : calculation

		[result, error_message]
	end



	def call
		return puts "\n\033[31m\e VIN code is required!" if @vin.nil?
		return puts "\n\033[31m\e VIN code size is invalid!" if @vin.size < 8

		result, error_message = calculate_check_digit(@vin)
		bool_digit_is_valid = @vin[8] == result
		digit_is_valid = bool_digit_is_valid ? "Valid" : "Invalid"

		puts "\nProvided VIN: #{@vin}"
		puts "Check Digit: " + digit_is_valid

		if is_not_valid
			current_error_message = error_message
			
			suggestions = []

			puts "\e[31mError Message: #{current_error_message}\n\e[0m"

			puts "Type a quantity to generate suggestions based on you original vin:"
			user_choice = STDIN.gets.chomp.to_i

			if user_choice >= 100
				puts "Are you sure that you want to generate #{user_choice} suggestions?"
				puts "[y] for yes \n[n] for default value"
				user_warning = STDIN.gets.chomp
				
				case user_warning.downcase
				when 'y'
				when 'n'
					user_choice = 5	
				else
					user_choice = 5	
					puts "\nInvalid choice, default value applied"
				end
			elsif user_choice == 0
				user_choice = 5	
				puts "\nInvalid choice, default value applied"
			end

			user_choice.times do
				new_suggestion = generate_suggested_vin
				result, error_message = calculate_check_digit(new_suggestion)
				new_suggestion[8] = result

				suggestions << new_suggestion
			end

			# I would get a list of all manufactures, and so on, to prevent redundance.
			# For today I will use uniq
			uniq_suggestions = suggestions.uniq

			puts "-------------------\nSuggested VINS:\n-------------------"
			uniq_suggestions.each {|each| puts "-> \033[32m#{each}\e[0m"}
			
			generate_report("
				\nError Message: #{current_error_message}\n
				\nSuggestions based on you original vin:", uniq_suggestions
			)
		elsif !bool_digit_is_valid
			@vin[8] = result
			puts "-------------------\nSuggested VIN:\n-------------------"
			puts "-> \033[32m#{@vin}\e[0m"

			generate_report("
				\nError Message: check digit is invalid\n
				\nSuggestion based on you original vin:", 
				@vin
			)
		else
			puts "This looks like a VALID vin!"
			generate_report("\nProvided VIN: #{@vin}\nThis looks like a VALID vin!")
		end
	end
end

VinValidator.new(ARGV[0]).call